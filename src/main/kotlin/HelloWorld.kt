import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun main(arg: Array<String>) {
    GlobalScope.launch {
        delay(1000)
        println("World")
    }
    print("Hello, ")
    Thread.sleep(2000) // if we remove it process will exit first
}