import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(arg: Array<String>) {
    var counter:Int =  0
// coroutines are light weight in nature
    runBlocking {
        repeat(1_00_000){
            launch {
                if(counter%100 == 0){
                    println("")
                }
                print("${counter++}, ")
            }
        }

    }
}