import kotlinx.coroutines.*
import kotlin.random.Random

fun main() {
    runBlocking {
        launch {
            val deferred = async {
                println("getting the first value ")
                getFirstValue()
            }
            val deferred2 = async {
                println("getting the second value")
                getSecondValue()
            }
            println("processing is happening")
            delay(500)
            println("wating for values ")
           val first =   deferred.await()
            val second = deferred2.await()
            println("addition of two values ${first + second}")
        }
    }
}

suspend fun getFirstValue(): Int {
    delay(1000)
    val value = Random.nextInt(1000)
    println("value of Int is $value")
    return value
}


suspend fun getSecondValue(): Int {
    delay(2000)
    val value = Random.nextInt(2000)
    println("value of Int is $value")
    return value
}