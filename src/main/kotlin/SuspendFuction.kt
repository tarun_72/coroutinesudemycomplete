import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

var functionCalls: Int = 0
fun main() {
    GlobalScope.launch {
        complete()
    }
    GlobalScope.launch {
        improveMessage()
    }
    print("Hello, ")
    Thread.sleep(2000)
    println("There have been $functionCalls calls so far ")
}

suspend fun complete() {
    delay(500)
    println("World ")
    functionCalls++
}

suspend fun improveMessage() {
    delay(1000)
    println("Suspend functions are cool :) ")
    functionCalls++
}