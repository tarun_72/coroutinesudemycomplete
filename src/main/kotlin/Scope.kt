import kotlinx.coroutines.*

fun main(arg: Array<String>) {
    println("Program execution will continue ")
    runBlocking { // this will block thread
        launch {
            delay(1000)
            println("task from runblocking")
        }
        GlobalScope.launch {// global scope
            delay(500)
            println("task from Global Scope ")
        }

        coroutineScope {
            launch {
                delay(1500)
                println("task from Coroutine Scope ")
            }
        }
        println("Program execution will terminated  ")


    }




}