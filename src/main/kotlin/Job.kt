import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(){
    println("lets start the job")
    runBlocking {
       val joblaunch1 =  launch {
           println("job is launch")
           delay(1000)
           println("delay ended ")
       }
        joblaunch1.invokeOnCompletion {
            println("job is completed or cancelled")
        }
    }


    Thread.sleep(2000)
}