import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(arg: Array<String>) {
    // data or var associated with coroutines
    runBlocking {
        launch(CoroutineName("tarun sharma ")) {
            println(message = "this is from coroutines(${this.coroutineContext.get(CoroutineName.Key)}")
        }
    }
}