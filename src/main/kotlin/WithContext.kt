import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

fun main() {

    runBlocking {
        launch(Dispatchers.Default) {
            println("current scope first of coroutines ${coroutineContext}")
            withContext(Dispatchers.IO){
                println("current scope second of coroutines ${coroutineContext}")

            }
            println("current scope third of coroutines ${coroutineContext}")

        }
    }
}