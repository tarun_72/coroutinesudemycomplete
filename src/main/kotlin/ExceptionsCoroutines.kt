import kotlinx.coroutines.*
import java.lang.IndexOutOfBoundsException

fun main() {
    runBlocking {
      /*  launch {
            println("this will block is slient killler")
            throw IndexOutOfBoundsException()
        }*/

//        this code will throw exception
    /*    val job =  launch {
            println("join will help us to detect the crash")
            throw IndexOutOfBoundsException()
        }
        job.join()*/

        println("lets handle the crash")
        val myExceptionHandler = CoroutineExceptionHandler{coroutineContext, throwable ->
            println("exception is handled ${throwable.message}")

        }
        val jobException =  GlobalScope.launch(myExceptionHandler) {
            println("join will help us to detect the crash")
            throw IndexOutOfBoundsException()
        }
        jobException.join()

        

    }
}
